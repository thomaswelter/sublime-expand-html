from expandhtml.expr_parser import parse
import sublime_plugin

class ExpandHtmlCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		for region in self.view.sel():
			if not region.empty():
				continue

			line = self.view.line(region)
			content = self.view.substr(line)

			try:
				snippet = parse(content)

			except ValueError:
				continue

			if not snippet:
				continue

			self.view.erase(edit, line)
			self.view.run_command('insert_snippet', {'contents': snippet})