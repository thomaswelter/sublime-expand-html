import re

matchers = {
	'id':				'\\#([_a-zA-Z]+[_a-zA-Z0-9-$]*)',
	'class':			'\\.([_a-zA-Z]+[_a-zA-Z0-9-$]*)',
	'tagName':			'([_a-zA-Z]+[_a-zA-Z0-9-$]*)',
	'attribute':		'\\[(.*?)\\]',
	'child':			'\\>',
	'sibling':			'\\+',
	'parent':			'\\^',
	'begin_group':		'\\(',
	'end_group':		'\\)',
	'content':			'\\{(.*?)\\}',
	'whitespace':		'\\s+',
	'repeat':			'\\*(\\d+)',
}

inferedTagName = {
	'ul': 'li',
	'em': 'span',
	'table': 'tr',
	'tr': 'td'
}

closedTags = ['meta', 'link', 'input']

inlineTags = ['span']

def consume(str):
	for name in matchers:
		m = re.match(matchers[name], str)
		if m:
			return (str[m.end():], name, m.groups())

	raise ValueError('this string doesn\'t match: ' + str)

def tokenize(str):
	tokens = []

	while len(str):
		str, name, groups = consume(str)
		tokens.append((name, groups))

	return tokens

def generateAst(tokens):
	tokens.reverse()
	root = []
	path = []
	groups = []
	current = root

	inSelector = False
	# Node: (tagName = '', props = {}, children = [])

	while len(tokens):
		name, values = tokens.pop()

		if not inSelector and (
			name == 'id' or
			name == 'class' or
			name == 'attribute' or
			name == 'repeat'):

			if len(path) and path[-1][0] in inferedTagName:
				tagName = inferedTagName[path[-1][0]]

			else:
				tagName = 'div'

			node = (tagName, {}, [])
			current.append(node)
			inSelector = True

		if name == 'tagName':
			node = (values[0], {}, [])
			current.append(node)
			inSelector = True

		elif name == 'id':
			if type(current[-1]) is str:
				raise ValueError('could not set id of textNode')

			current[-1][1]['id'] = values[0]

		elif name == 'class':
			if type(current[-1]) is str:
				raise ValueError('could not set class of textNode')

			if 'class' in current[-1][1]:
				classes = current[-1][1]['class']
				classes.split(' ')
				classes.append(values[0])
				classes.join(' ')
				current[-1][1]['class'] = classes

			else:
				current[-1][1]['class'] = values[0]


		elif name == 'attribute':
			for k, v in re.findall('([_a-zA-Z]+[_a-zA-Z0-9-]*)="(.*?)"', values[0]):
				current[-1][1][k] = v

		elif name == 'repeat':
			node = current[-1]
			for i in range(int(values[0]) -1):
				current.append(node)

		elif name == 'child':
			if not len(current):
				raise ValueError('> must be preceded by a selector')

			if type(current[-1]) is str:
				raise ValueError('could not get child of textNode')

			path.append(current[-1])
			current = current[-1][2]
			inSelector = False

		elif name == 'sibling':
			inSelector = False
			continue

		elif name == 'parent':
			if not len(path):
				raise ValueError('could not go up more')

			if len(path) == 1:
				current = root

			else:
				current = path.pop()[2]

			inSelector = False

		elif name == 'begin_group':
			groups.append(current)
			inSelector = False

		elif name == 'end_group':
			if not len(groups):
				raise ValueError('group was not opened')

			current = groups.pop()
			inSelector = False

		elif name == 'content':
			if not len(current):
				raise ValueError('{} must be preceded by a selector')

			if type(current[-1]) is str:
				raise ValueError('could not get child of textNode')

			current[-1][2].append(values[0])

		elif name == 'whitespace':
			continue

		else:
			return False

	return root

def computeVars(nodes, n = 1):
	l = []

	for i, node in enumerate(nodes):
		if type(node) == str:
			l.append(re.sub('\\$+', str(n), node))

		else:
			tagName = re.sub('\\$+', str(n), node[0])

			props = {}
			for k, v in node[1].items():
				props[k] = re.sub('\\$+', str(i +1), v)

			newNode = (tagName, props, computeVars(node[2], i +1))
			l.append(newNode)

	return l

def walk(nodes, d = 0):
	l = []
	for node in nodes:
		l += [(d, node, True)]

		if type(node) != str:
			l += walk(node[2], d +1) + [(d, node, False)]

	return l

def toSnippet(nodes):
	content = ''
	wasBlock = True
	tskey = 1

	for depth, node, begin in walk(nodes):
		if type(node) == str:
			indent = (depth * '\t') if wasBlock else ''
			content += indent + node
			wasBlock = False
			continue

		name, props, children = node

		if name in closedTags and not begin:
			continue

		if len(children) == 0 and not begin:
			indent = ((depth +1) * '\t') if wasBlock else ''
			content += indent + '$' + str(tskey)
			tskey += 1
			wasBlock = False

		# indent + newline
		isBlock = not name in inlineTags
		indent = ('' if wasBlock else '\n') + (depth * '\t') if isBlock else ''
		nl = '\n' if isBlock else ''

		if begin:
			propstr = ''
			for k, v in props.items():
				if not len(v):
					v = '$' + str(tskey)
					tskey += 1

				propstr += ' ' + k + '="' + v + '"'

			# closed tag
			if name in closedTags:
				content += indent + '<' + name + propstr + ' />' + nl
				wasBlock = isBlock
				continue

			content += indent + '<' + name + propstr + '>' + nl

		else:
			content += indent + '</' + name + '>' + nl

		wasBlock = isBlock

	return content + '$' + str(tskey)

def parse(str):
	tokens = tokenize(str)
	ast = generateAst(tokens)
	if not ast: return False
	ast = computeVars(ast)
	string = toSnippet(ast)

	return string