import unittest
from textwrap import dedent
from expr_parser import parse

class TestParse(unittest.TestCase):

	def test_simple(self):
		self.assertEqual(parse('div'), dedent(
			"""\
			<div>
				$1
			</div>
			$2"""
		))

	def test_child(self):
		self.assertEqual(parse('div>section'), dedent(
			"""\
			<div>
				<section>
					$1
				</section>
			</div>
			$2"""
		))

	def test_multiplynodes(self):
		self.assertEqual(parse('ul>*2'), dedent(
			"""\
			<ul>
				<li>
					$1
				</li>
				<li>
					$2
				</li>
			</ul>
			$3"""
		))	

	def test_selfclosing(self):
		self.assertEqual(parse('input[value="somevalue"]'), dedent(
			"""\
			<input value="somevalue" />
			$1"""
		))	

	def test_content(self):
		self.assertEqual(parse('div{some inner text}'), dedent(
			"""\
			<div>
				some inner text
			</div>
			$1"""
		))	

	def test_customclass(self):
		self.assertEqual(parse('.customclass'), dedent(
			"""\
			<div class="customclass">
				$1
			</div>
			$2"""
		))	

	def test_siblings(self):
		self.assertEqual(parse('p+p'), dedent(
			"""\
			<p>
				$1
			</p>
			<p>
				$2
			</p>
			$3"""
		))	

	def test_groups(self):
		self.assertEqual(parse('(div.group>p{group b}) + (div.group2>p{group a})'), dedent(
			"""\
			<div class="group">
				<p>
					group b
				</p>
			</div>
			<div class="group2">
				<p>
					group a
				</p>
			</div>
			$1"""
		))	

	def test_gotoparent(self):
		self.assertEqual(parse('#parent>p + p^#sibling'), dedent(
			"""\
			<div id="parent">
				<p>
					$1
				</p>
				<p>
					$2
				</p>
			</div>
			<div id="sibling">
				$3
			</div>
			$4"""
		))	

	def test_attributes(self):
		self.assertEqual(parse('div[onclick="doSomething()"]'), dedent(
			"""\
			<div onclick="doSomething()">
				$1
			</div>
			$2"""
		))	

	def test_infertabletagnames(self):
		self.assertEqual(parse('table>*2>*2'), dedent(
			"""\
			<table>
				<tr>
					<td>
						$1
					</td>
					<td>
						$2
					</td>
				</tr>
				<tr>
					<td>
						$3
					</td>
					<td>
						$4
					</td>
				</tr>
			</table>
			$5"""
		))	

	def test_variables(self):
		self.assertEqual(parse('ul>*5{item $}'), dedent(
			"""\
			<ul>
				<li>
					item 1
				</li>
				<li>
					item 2
				</li>
				<li>
					item 3
				</li>
				<li>
					item 4
				</li>
				<li>
					item 5
				</li>
			</ul>
			$1"""
		))	

if __name__ == '__main__':
	unittest.main()