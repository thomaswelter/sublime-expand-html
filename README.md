# Sublime expand html
![screen capture](screen-capture.gif)

A sublime text plugin that expands html like Emmet written in pyhton.

Clone the repo into your sublime packages folder to use it. The trigger is set to **[ctrl + e]**.

This plugin inserts a snippet with tab-stops in handy places.

# Examples
- `div`
  create div element

- `.some-class`
  create div with class

- `#some-id`
  create div with id

- `.parent > .child`
  parent div with child div

- `.a > .child-of-a ^ .b`
  `^` goes one level up

- `.item > h3 + p`
  h3 and p will be siblings

- `ul > .item`
  infer tagname, ul will always contain li tags

- `ul > li *3`
  repeat li 3 times
  
- `ul > li.item-$ *3 {content of number $}`
  repeat li and set classname and content using index

- `table > .row-$ *3 > .column-$ *3`
  3x3 matrix. infer tr and td tagnames and add class names.

- `a[href="page.html"] {goto page}`
  set attributes